import random
from matplotlib import patches, pyplot as plt


class Question:
    def __init__(self):
        self.title = ""
        self.difficulty = 0
        self.points = []
        self.predicates = []


class Point:
    """
    Class representing a point. It specifies the point's position on the diagram. The method  is_inside checks if the
    given point is inside the predicate object passed as a parameter.
    """
    def __init__(self):
        self.x = random.randrange(2, 98)
        self.y = random.randrange(2, 98)

    def is_inside(self, predicate):  # predicate:
        # for every point do this
        # compute whether it is inside or not following comp graphics method.
        # assign the truth or false value to the point.
        if predicate.x < self.x < (predicate.x + predicate.width) and predicate.y < self.y < (
                predicate.y + predicate.height):
            return True
        return False


class Predicate:
    """
    Class representing a predicate, it specifies its area and position on the diagram.
    Colours are orange, light blue, and purple.
    """
    colours = ["#f0954a", "#4abbf0", "#a95aa1"]

    def __init__(self):
        self.width = random.randrange(20, 60)
        self.height = random.randrange(20, 60)
        self.x = random.randrange(5, 95 - self.width)
        self.y = random.randrange(5, 95 - self.height)
        self.centerx = self.x + self.width / 2
        self.centery = self.y + self.height / 2


def is_inside(points, predicate):
    """
    :param points: list of point objects.
    :param predicate: Object representing a predicate.
    :return List of the points that are inside the predicate.
    """
    # for every point do this
    # compute whether it is inside or not following comp graphics method.
    # assign the truth or false value to the point.
    inside = []
    for point in points:
        if predicate.x < point.x < (predicate.x + predicate.width) \
                and predicate.y < point.y < (
                predicate.y + predicate.height):
            inside.append(point)
    return inside


def true_formulas(points, predicates):
    """
    :param points: list of point objects.
    :param predicates: list of predicate objects.
    :return: list of formulas that evaluate to True according to the diagram.
    """
    formulas = []
    n_points = len(points)
    if len(predicates) == 1:
        # P is empty
        if all(all(not point.is_inside(predicate) for predicate in predicates) for point in points):
            formulas.extend(["∀x(¬P(x))", "∃x(¬P(x))", "¬∀x(P(x))", "¬∃x(P(x))"])
            return formulas
        # All points are inside P
        if all(all(point.is_inside(predicate) for predicate in predicates) for point in points):
            formulas.extend(["∀x(P(x))", "¬∀x(P(x))", "∃x(P(x))", "¬∃x(P(x))", "¬∀x(¬P(x))", "¬∃x(¬P(x))"])
            return formulas
        # Some points are inside P
        formulas.extend(["∃x(P(x))", "∃x(¬P(x))"])
        return formulas

    # All predicates are empty.
    if all(all(not point.is_inside(predicate) for predicate in predicates) for point in points):
        formulas.extend(["∀x(¬P(x) ^ ¬Q(x)", "∀x(¬P(x) ∨ ¬Q(x)", "∃x(¬P(x) ^ ¬Q(x)", "∃x(¬P(x) ∨ ¬Q(x)"])
        return formulas
    # P is not empty Q is empty.
    if is_inside(points, predicates[0]) and not is_inside(points, predicates[1]):
        formulas.extend(["∃x(P(x) ∧ ¬Q(x))", "∃x(P(x) ∨ Q(x))", "∃x(P(x) ∨ ¬Q(x))", "∃x(¬P(x) ∨ ¬Q(x))"])
        return formulas
    # P is empty Q is not empty.
    if not is_inside(points, predicates[0]) and is_inside(points, predicates[1]):
        formulas.extend(["∃x(¬P(x) ∧ Q(x))", "∃x(P(x) ∨ Q(x))", "∃x(¬P(x) ∨ Q(x))", "∃x(¬P(x) ∨ ¬Q(x))"])
        return formulas

    # # P is not empty Q is not empty.
    if is_inside(points, predicates[0]) and is_inside(points, predicates[1]):
        intersection = set(is_inside(points, predicates[0])).intersection(is_inside(points, predicates[1]))
        # Check intersection is not empty.
        if intersection:
            if n_points == len(intersection):
                formulas.extend(["∃x(P(x) ∧ Q(x))", "∃x(P(x) ∨ Q(x))"])
                return formulas

            formulas.extend(["∃x(P(x) ∧ Q(x))", "∃x(P(x) ∨ Q(x))", "∃x(¬P(x) ∨ ¬Q(x))"])
            return formulas
        # Check if all points are inside P and Q.
        if n_points <= len(is_inside(points, predicates[0])) + len(
                is_inside(points, predicates[1])):  # does not work when the intersection is not empty.
            formulas.extend(["∀x(P(x) ∨ Q(x)", "∀x∀y(P(x) ∨ Q(y)", "∀x∀y(P(x) ∧ Q(y)"])
            return formulas

        formulas.extend(["∃x(¬P(x) ∧ Q(x))", "∃x(P(x) ∧ ¬Q(x))", "∃x(P(x) ∨ Q(x))",
                         "∃x(P(x) ∨ ¬Q(x))", "∃x(¬P(x) ∨ Q(x))"])
        return formulas

    return formulas


def false_formulas(points, predicates, true_formulas):
    """
    :param points: list of point objects.
    :param predicates: list of predicate objects.
    :param true_formulas: list of formulas that evaluate to True according to the diagram.
    :return: list of formulas that evaluate to False according to the diagram.

    This function generates three random formula using the string format method, they are compared against
    the list true formulas to ensure they do not evaluate to True.
    """
    quantifiers = ['∀', '∃']
    operations = ['∧', '∨']
    variables = ['x', 'y']
    negation = ["¬", ""]

    formulas = []
    n_points = len(points)
    n_predicates = len(predicates)

    while len(formulas) < 3:
        if n_predicates > 1:
            if n_points > 1:
                # 2 quantifiers 2 predicates
                operation = random.choice(operations)
                variable1 = random.choice(variables)
                variable2 = random.choice(variables)
                quantifier1 = random.choice(quantifiers)
                quantifier2 = random.choice(quantifiers)

                formula = "{}{}{} {}{}{}({}P({}) {} {}Q({}))".format(random.choice(negation), quantifier1, variable1,
                                                                     random.choice(negation), quantifier2, variable2,
                                                                     random.choice(negation), variable1, operation,
                                                                     random.choice(negation), variable2)

                if formula not in formulas and formula not in true_formulas:
                    formulas.append(formula)
                    continue

            # 1 quantifier 2 predicates
            quantifier1 = random.choice(quantifiers)
            operation = random.choice(operations)

            formula = "{}{}x({}P(x) {} {}Q(x))".format(random.choice(negation), quantifier1, random.choice(negation),
                                                       operation, random.choice(negation))
            if formula not in formulas and formula not in true_formulas:
                formulas.append(formula)
                continue

        if n_points > 1:
            # 2 quantifiers 1 predicate
            operation = random.choice(operations)
            quantifier1 = random.choice(quantifiers)
            quantifier2 = random.choice(quantifiers)

            formula = "{}{}x {}{}y({}P(x) {} {}P(y))".format(random.choice(negation), quantifier1,
                                                             random.choice(negation), quantifier2,
                                                             random.choice(negation), operation,
                                                             random.choice(negation))
            if formula not in formulas and formula not in true_formulas:
                formulas.append(formula)
                continue

        # 1 quantifier 1 predicate
        formula = "{}{}x({}P(x))".format(random.choice(negation), random.choice(quantifiers), random.choice(negation))

        if formula not in formulas and formula not in true_formulas:
            formulas.append(formula)
        elif formula in formulas and formula not in true_formulas:
            formulas.append("∃x(¬P(x) ∧ P(x))")

    return formulas


def generate_answers(points, predicates):
    """
    :param points:
    :param predicates:
    :return: List of 4 formulas to be displayed as the possible answers in the exercise.
    """
    answers = {}
    correct_formula = random.choice(true_formulas(points, predicates))
    answers[correct_formula] = True  # add correct answer to the dictionary.

    incorrect_formulas = false_formulas(points, predicates, correct_formula)
    for formula in incorrect_formulas:
        answers[formula] = False  # add incorrect answers to the dictionary.
    return answers


def get_difficulty(points, predicates):
    """
    The difficulty level depends on the number of points and predicates on the diagram.
    :param points:
    :param predicates:
    :return: Number from 1 to 4 detailing the difficulty level.
    """
    n_points = len(points)
    n_predicates = len(predicates)

    if n_points <= 2 and n_predicates < 2:
        return 1
    elif n_points > 2 and n_predicates < 2:
        return 2
    elif n_points <= 2 and n_predicates >= 2:
        return 3
    elif n_points > 2 and n_predicates >= 2:
        return 4
    return


def generate_exercise():
    """
    Generates the exercise
    :return: the diagram, list of points, list of predicates, list of possible answers.
    """

    # Generate the plot
    fig, ax = plt.subplots()
    plt.xlim(0, 100)
    plt.ylim(0, 100)
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)

    predicates = []
    points = []
    n_predicates = random.randrange(1, 3)
    n_points = random.randrange(1, 5)

    # Populate plot with the predicates
    for i in range(n_predicates):
        predicate = Predicate()
        predicates.append(predicate)
        ax.add_patch(patches.Rectangle((predicate.x, predicate.y), predicate.width, predicate.height,
                                       edgecolor=predicate.colours[i], fill=False, lw=3))
        ax.text(predicate.centerx, predicate.centery, chr(80 + i), fontsize="x-large", color=predicate.colours[i],
                fontweight="black")

    # Populate plot with the points
    for i in range(n_points):
        point = Point()
        points.append(point)
        ax.plot(point.x, point.y, 'ro')

    answers = generate_answers(points, predicates)
    plt.close()
    return fig, points, predicates, answers
