from django.db import models


# Create your models here.
class Student(models.Model):
    name = models.CharField(max_length=30)
    surname = models.CharField(max_length=30)
    email = models.CharField(max_length=254, unique=True)
    password = models.CharField(max_length=30)

    def __str__(self):
        return u'%s %s' % (self.name, self.email)


class Exercise(models.Model):
    question = models.CharField(max_length=300)
    answer = models.CharField(max_length=300)


class Result(models.Model):
    student = models.ForeignKey(Student, on_delete=models.DO_NOTHING)
    exercise = models.ForeignKey(Exercise, on_delete=models.DO_NOTHING)
    result = models.BinaryField(default=False)
