# Generated by Django 3.1.7 on 2021-02-24 17:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tutorapp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Exercise',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('question', models.CharField(max_length=300)),
                ('answer', models.CharField(max_length=300)),
            ],
        ),
        migrations.CreateModel(
            name='Result',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('result', models.BinaryField(default=False)),
                ('exercise', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='tutorapp.exercise')),
                ('student', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='tutorapp.student')),
            ],
        ),
    ]
