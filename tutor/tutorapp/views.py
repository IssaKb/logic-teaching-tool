import base64
from django.shortcuts import render
from io import BytesIO
from tutorapp.logic import *

# Generates the diagram for the exercise and passes it on to the HTML template to be displayed on the website.
def get_question(request):
    exercise = generate_exercise()
    fig = exercise[0]
    q = Question()
    buffer = BytesIO()
    answers = []

    q.points = exercise[1]
    q.predicates = exercise[2]
    q.title = "Select the correct formula"
    q.difficulty = get_difficulty(q.points, q.predicates)

    fig.savefig(buffer, format='png')
    buffer.seek(0)
    image = buffer.getvalue()
    buffer.close()

    graphic = base64.b64encode(image)
    graphic = graphic.decode('utf-8')

    answers.extend(exercise[3].items())

    random.shuffle(answers)
    return render(request, 'exercises.html', {'graphic': graphic, 'question': q, "answers": answers})


def about(request):
    return render(request, 'about.html')


def homepage(request):
    return render(request, 'index.html')


def help(request):
    return render(request, 'help.html')
