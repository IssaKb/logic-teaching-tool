// Checks if they selected the correct answer.
window.onload = function (){
    let submitBtn = document.getElementById("confirm-answer-button")
    submitBtn.onclick = function getAnswer() {
        window.onbeforeunload = null;
        const rbs = document.querySelectorAll('input[name="answer"]');
        let selectedAnswer;

        for (const rb of rbs) {
            if (rb.checked) {
                selectedAnswer = rb;
                break;
            }
        }

        if (selectedAnswer == undefined){
            alert("Select an answer");
            $('#confirmAnswerModal').modal('hide')
        }

        else if (selectedAnswer.value == "True"){
            alert("Congratulations you selected the correct answer!");
            $('#confirmAnswerModal').modal('hide')
            window.location.replace("/exercise/")
        }

        else if(selectedAnswer.value == "False"){
            alert("Incorrect answer. Try again.");
            $('#confirmAnswerModal').modal('hide')
        }
    };

//Asks them if they are sure they want to skip the exercise.
    let skipBtn = document.getElementById("skip-exercise-button")
    skipBtn.onclick = window.onbeforeunload = function() {
        return true;
        }
}